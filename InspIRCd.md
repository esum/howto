# InspIRCd cheat sheet

## Modes

InspIRCd supports 5 types of modes :

| Type          | Parameter when adding        | Parameter when removing      | Can be set multiple times        | Description                                                                     |
|---------------|------------------------------|------------------------------|----------------------------------|---------------------------------------------------------------------------------|
| Switch        | No                           | No                           | No                               | Toggles the behaviour of a feature on a channel or user.                        |
| Parameter     | Yes                          | No                           | No                               | Enables and configures the behaviour of a feature on a channel or user.         |
| ParameterBoth | Yes                          | Yes                          | No                               | The same as a Parameter mode only the parameter must be specified to remove it. |
| Prefix        | Yes; channel member nickname | Yes; channel member nickname | Yes; once per channel member     | Grants/revokes a status rank to the user specified in the parameter.            |
| List          | Yes                          | Yes                          | Yes; up to the maximum list size | Adds/removes entries from a list.                                               |

### Channel modes

| Character | Name          | Type          | Parameter syntax    | Usable by         | Module | Description |
|-----------|---------------|---------------|---------------------|-------------------|--------|-------------|
| `A`       | allowinvite   | Switch        |                     | Channel operators | allowinvite | Allows unprivileged users to use the `INVITE` command. |
| `B`       | anticaps      | Parameter     | type:minlen:percent | Channel operators | anticaps | Enables blocking excessively capitalised messages (type can be `ban`, `block`, `mute`, `kick` or `kickban`). | 
| `B`       | blockcaps     | Switch        |                     | Channel operators | blockcaps | Enables blocking excessively capitalised messages. |
| `b`       | ban           | List          | mask                | Channel operators |        | Bans users matching &lt;mask&gt; from joining the channel. |
| `C`       | noctcp        | Switch        |                     | Channel operators | noctcp | Enables blocking channel messages that contain CTCPs. |
| `c`       | blockcolor    | Switch        |                     | Channel operators | blockcolor | Enables blocking messages that contain IRC formatting codes. |
| `D`       | delayjoin     | Switch        |                     | Channel operators | delayjoin | Prevents users from receiving `JOIN` messages until the joining user speaks. |
| `d`       | delaymsg      | Parameter     | seconds             | Channel operators | delaymsg | Prevents newly joined users from speaking until &lt;seconds&gt; seconds have passed. |
| `E`       | repeat        | Parameter     | (~\|*)lines:seconds | Channel operators | repeat | Configures the messages that should be considered a repeat. If prefixed with ~ the messages are blocked. If prefixed with * then offending users are banned. If not prefixed then offending users are kicked. |
| `e`       | banexception  | List          | mask                | Channel operators | banexception | Exempts users matching &lt;mask&gt; from the `b` (ban) channel mode.
| `F`       | nickflood     | Parameter     | changes:seconds     | Channel operators | nickflood | Prevents more than &lt;changes&gt; nickname changes in the last &lt;seconds&gt; seconds. |
| `f`       | flood         | Parameter     | (*)lines:seconds    | Channel operators | messageflood | Kicks users who send more than &lt;lines&gt; messages in the last &lt;seconds&gt; seconds. If prefixed with * then offending users are also banned. |
| `g`       | filter        | List          | glob                | Channel operators | chanfilter | Prevents users from sending messages that match &lt;glob&gt;. |
| `H`       | history       | Parameter     | count:period        | Channel operators | chanhistory | Sends up to &lt;count&gt; messages from the last &lt;period&gt; on join. |
| `I`       | invex         | List          | mask                | Channel operators | inviteexception | Exempts users matching &lt;mask&gt; from the `i` (inviteonly) channel mode. |
| `i`       | inviteonly    | Switch        |                     | Channel operators |        | Prevents users from joining the channel without an invite. |
| `J`       | kicknorejoin  | Parameter     | seconds             | Channel operators | kicknorejoin | Prevents who have been kicked from rejoining until &lt;seconds&gt; seconds have passed. |
| `j`       | joinflood     | Parameter     | joins:seconds       | Channel operators | joinflood | Prevents more than &lt;joins&gt; joins in the last &lt;seconds&gt; seconds. |
| `k`       | key           | ParameterBoth | key                 | Channel operators |        | Prevents users from joining the channel who have not specified the &lt;key&gt; password. |
| `L`       | redirect      | Parameter     | channel             | Channel operators | redirect | Redirects all new users to &lt;channel&gt; when the user limit is reached. |
| `l`       | limit         | Parameter     | count               | Channel operators |        | Allows no more than &lt;count&gt; users to join the channel. |
| `M`       | regmoderated  | Switch        |                     | Channel operators | services_account | Prevents users who are not logged into a services account from speaking in the channel. |
| `m`       | moderated     | Switch        |                     | Channel operators |        | Prevents users without a prefix rank from messaging the channel. |
| `N`       | nonick        | Switch        |                     | Channel operators | nonicks | Prevents users from changing their nickname whilst in the channel. |
| `n`       | noextmsg      | Switch        |                     | Channel operators |        | Prevents users who are not in the channel from messaging the channel. |
| `O`       | operonly      | Switch        |                     | Server operators  | operchans | Prevents non-server operators from joining the channel. |
| `o`       | op            | Prefix        | nick                | Channel operators |        | Grants channel operator status to &lt;nick&gt;. |
| `P`       | permanent     | Switch        |                     | Server operators  | permchannels | Prevents the channel from being deleted when the last user leaves. |
| `p`       | private       | Switch        |                     | Channel operators |        | Hides the channel in `WHOIS` from people who are not a member. You probably want the `s` (secret) channel mode rather than this. |
| `Q`       | nokick        | Switch        |                     | Channel operators | nokicks | Prevents privileged users from using the `KICK` command. |
| `R`       | reginvite     | Switch        |                     | Channel operators | services_account | Prevents users who are not logged into a services account from joining the channel. |
| `r`       | c_registered  | Switch        |                     | Channel operators | services_account | Marks the channel as being registered. |
| `s`       | secret        | Switch        |                     | Channel operators |        | Hides the channel in `WHOIS` and `LIST` from people who are not a member. |
| `T`       | nonotice      | Switch        |                     | Channel operators | nonotice | Enables blocking messages sent with the `NOTICE` command. |
| `t`       | topiclock     | Switch        |                     | Channel operators |        | Prevents non-channel operators from changing the channel topic. |
| `u`       | auditorium    | Switch        |                     | Channel operators | auditorium | Enables auditorium mode : hides unprivileged users in a channel from each other. |
| `v`       | voice         | Prefix        | nick                | Channel operators |        | Grants channel voice status to &lt;nick&gt;. |
| `w`       | autoop        | List          | status:mask         | Channel operators | autoop | Grants the &lt;status&gt; rank to users matching &lt;mask&gt; on join. |
| `X`       | exemptchanops | List          | restriction:mode    | Channel operators | exemptchanops | Exempts users with the &lt;mode&gt; prefix mode or higher from &lt;restriction&gt;. |
| `Y`       | official-join | Prefix        | nick                | Server operators  | ojoin  | Grants channel official-join status to &lt;nick&gt;. |
| `y`       | operprefix    | Prefix        | nick                | Server operators  | operprefix | Grants channel operprefix status to &lt;nick&gt;. |
| `z`       | sslonly       | Switch        |                     | Channel operators | sslmodes | Prevents users who are not connected using TLS (SSL) from joining the channel. | 

### User modes

| Character | Name            | Type          | Parameter syntax | Usable by        | Module | Description |
|-----------|-----------------|---------------|------------------|------------------|--------|-------------|
| `B`       | bot             | Switch        |                  | Anyone           | botmode | Marks the user as a bot. |
| `c`       | deaf_commonchan | Switch        |                  | Anyone           | commonchans | Requires other users to have a common channel before they can message this user. |
| `D`       | privdeaf        | Switch        |                  | Anyone           | deaf   | Prevents the user from receiving private messages. |
| `d`       | deaf            | Switch        |                  | Anyone           | deaf   | Prevents the user from receiving channel messages. |
| `g`       | callerid        | Switch        |                  | Anyone           | callerid | Enables whitelisting of who can message the user. |
| `H`       | hideoper        | Switch        |                  | Server operators | hideoper | Hides the user's server operator status from unprivileged users. |
| `h`       | helpop          | Switch        |                  | Server operators | helpop | Marks the user as being available for help. |
| `I`       | hidechans       | Switch        |                  | Anyone           | hidechans | Hides the channels the user is in from their `WHOIS` response. |
| `i`       | invisible       | Switch        |                  | Anyone           |        | Marks the user as invisible. |
| `k`       | servprotect     | Switch        |                  | Servers          | servprotect | Protects services pseudoclients against kicks, kills, and channel prefix mode changes. |
| `N`       | nohistory       | Switch        |                  | Anyone           | chanhistory | Disables receiving channel history on join. |
| `o`       | oper            | Switch        |                  | Server operators |        | Marks the user as a server operator (can only be set by the server). |
| `R`       | regdeaf         | Switch        |                  | Anyone           | services_account | Prevents users who are not logged into a services account from messaging the user. |
| `r`       | u_registered    | Switch        |                  | Anyone           | services_account | Marks the user as being logged into a services account. |
| `s`       | snomask         | Parameter     |                  | Server operators |        | Enables receiving the specified types of server operator notice. |
| `T`       | u_noctcp        | Switch        |                  | Anyone           | noctcp | Enables blocking private messages that contain CTCPs. |
| `W`       | showwhois       | Switch        |                  | Configurable     | showwhois | Informs the user when someone does a `WHOIS` query on their nick. |
| `w`       | wallops         | Switch        |                  | Anyone           |        | Enables receiving `WALLOPS` messages from server operators. |
| `x`       | cloak           | Switch        |                  | Anyone           | cloaking | Enables hiding of the user's hostname. |
| `z`       | sslqueries      | Switch        |                  | Anyone           | sslmodes | Prevents messages from being sent to or received from a user that is not connected using TLS (SSL). |
